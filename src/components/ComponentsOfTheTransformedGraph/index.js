import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ComponentsOfTheTransformedGraph extends Component {
  static propTypes = {
    adjacencyMatrix : PropTypes.arrayOf(PropTypes.array),
  };

  render() {
    const { array } = this.props;
    console.log(array);
    return (
      <div className="container">
        <h3>Компоненты связаности</h3>
        <p>{array.map(j => `Компонента связоности: [ ${j.join(', ')} ]`).join(', ')}</p>
      </div>
    );
  }
}
