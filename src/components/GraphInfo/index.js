import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  getDegreesOfVertices,
  getIsolatedVertices,
  getPendantVertices,
  getPendantVerges,
  getLoops,
  getMultipleVerges } from '../../simple-graph2';

export default class GraphInfo extends Component {
  static propTypes = {
    content : PropTypes.string,
  };

  render() {
    const { content } = this.props;
    console.log(getDegreesOfVertices(content));
    return (
      <div className="container">
        <table>
          <tbody>
            <tr>
              <td>Степени всех вершин</td>
              <td>{[...getDegreesOfVertices(content).keys()].map(key => (
                <span>
                  Вершина: {key} степень: {getDegreesOfVertices(content).get(key).length || 0},
                </span>
              ))}</td>
            </tr>
            <tr>
              <td>Изолированные вершины</td>
              <td>{getIsolatedVertices(content).join(', ')}</td>
            </tr>
            <tr>
              <td>Висячие вершины</td>
              <td>{getPendantVertices(content).join(', ')}</td>
            </tr>
            <tr>
              <td>Висячие ребра</td>
              <td>{getPendantVerges(content).map(i => i.join('-')).join(', ')}</td>
            </tr>
            <tr>
              <td>Вершины, в которых имеются петли</td>
              <td>{getLoops(content).map(i => `Вершина: ${i.a} Кратность: ${i.b}`).join(', ')}</td>
            </tr>
            <tr>
              <td>Кратные ребра</td>
              <td>{getMultipleVerges(content).map(i => `Ребро: ${i.a} Кратность: ${i.b}`).join(', ')}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
