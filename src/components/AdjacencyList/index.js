import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AdjacencyList extends Component {
  static propTypes = {
    adjacencyList: PropTypes.object,
    title: PropTypes.string,
  };

  render() {
    const { adjacencyList, title } = this.props;
    return (
      <div className="container">
        <h3>{title}</h3>
        <table>
          <tbody>
            {[...adjacencyList.keys()].map(i => (
              <tr>
                <td>{i}</td>
                <td>[{adjacencyList.get(i).join(', ')}]</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
