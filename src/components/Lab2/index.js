import React, { Component } from 'react';
import {
  graphToSimpleForm,
  getEdges,
  getEdgeGraph,
  getNodesForEdge,
  getEdgesForEdge,
  checkByBFS,
  getNodes,
  NP,
  matching,
  dominantSetOfVertices,
  dominantSetOfEdge,
  createDopGragh,
} from '../../simple-graph2/index';
import PropTypes from "prop-types";
import GraphAwesome from "../GraphAwesome";

class Lab2 extends Component {
  static propTypes = {
    content : PropTypes.string,
  };

  render() {
    const { content } = this.props;
    console.log(dominantSetOfEdge(content));
    return (
      <div className="App">
        {content && <GraphAwesome title="Исходный Граф" graph={{
          nodes: getNodes(graphToSimpleForm(content)),
          edges: getEdges(graphToSimpleForm(content)),
        }} />}
        {content && <GraphAwesome title="Реберный Граф" graph={{
          nodes: getNodesForEdge(getEdgeGraph(content)),
          edges: getEdgesForEdge(getEdgeGraph(content)),
        }} />}
        {content && <h3>{checkByBFS(content) ? 'Граф двудольный' : 'Граф недвудольный'}</h3>}
        {content && <h3>Наименьшее врешинное покрытие {NP(content).a.join(', ')}</h3>}
        {content && <h3>Максимальноенезависемое множество {NP(content).b.join(', ')}</h3>}
        {content && <div>
          <h3>Максимальной кликa дополнительного графа [{createDopGragh(content).b.join(', ')}]</h3>
          <h3>Максимальное независимое множество вершин исходного графа [{createDopGragh(content).c.join(', ')}]</h3>
          <GraphAwesome title="Дополнительный граф"
                        graph={{
                          nodes: getNodes(createDopGragh(content).a),
                          edges: getEdges(createDopGragh(content).a),
                        }}
          />
        </div>}
        {content && <h3>Максимальноенезависемое паросочетание {matching(content).a.map(i => i.join('-')).join(', ')}</h3>}
        {content && <h3>Наименьшее рёберное покрытие {matching(content).b.map(i => i.join('-')).join(', ')}</h3>}
        {content && <GraphAwesome
          title="Наименьшее вершинное покрытие"
          graph={{
          nodes: getNodes(graphToSimpleForm(content), dominantSetOfVertices(content)),
          edges: getEdges(graphToSimpleForm(content)),
        }} />}
        {content && <GraphAwesome
          title="Наименьшее вершинное покрытие"
          graph={{
            nodes: getNodes(graphToSimpleForm(content)),
            edges: getEdges(graphToSimpleForm(content), dominantSetOfEdge(content)),
          }} />}
        </div>
    );
  }
}

export default Lab2;
