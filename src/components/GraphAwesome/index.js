import React, { Component } from 'react';
import PropTypes from 'prop-types';

var Graph = require('vis-react').default;

class GraphAwesome extends Component {
  static propTypes = {
    title : PropTypes.string,
    graph : PropTypes.any,
  };

  render() {
    const { title, graph } = this.props;
    return (
      <div>
        <h3>{title}</h3>
        <Graph graph={graph} options={{ width: '90%', height: '700px' }} events={{}} />
      </div>
    );
  }
}

export default GraphAwesome;
