import React, { Component } from 'react';
import {
  getNodesWithSize,
  stringToAdjacencyListVithSize,
  getEdgesWithSize,
  myPrimMST,
  myKruskal
} from '../../simple-graph2/index';
import PropTypes from "prop-types";
import GraphAwesome from "../GraphAwesome";

class Lab4 extends React.Component {
  constructor() {
    super();
    this.state = {
      result: null,
    };
  }

  componentDidMount() {
    const { content } = this.props;
    if (content) {
      console.log(123)
      myKruskal(content, (result) => {
        console.log('result', result.mstArray.map(a => `${a[0] + 1}, ${a[1] + 1}`));
        this.setState({ result: result.mstArray.map(a => `${a[0] + 1}, ${a[1] + 1}`) });
      });
    }
  }

  render() {
    const { content } = this.props;
    const { result } = this.state;
    const arr = myPrimMST(content).map(a => `${a[0]}, ${a[1]}`);
    return (
      <div className="App">
        <GraphAwesome
          title="Исхоный граф"
          graph={{
            nodes: getNodesWithSize(stringToAdjacencyListVithSize(content)),
            edges: getEdgesWithSize(stringToAdjacencyListVithSize(content), arr),
        }}/>
        {content && result && <div>
          <GraphAwesome
            title="Исхоный граф"
            graph={{
              nodes: getNodesWithSize(stringToAdjacencyListVithSize(content)),
              edges: getEdgesWithSize(stringToAdjacencyListVithSize(content), arr),
            }}/>
        </div>}
      </div>
    );
  }
}

export default Lab4;
