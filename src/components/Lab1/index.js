import React, { Component } from 'react';
import {
  createAdjacencyList,
  stringToAdjacencyMatrix,
  findComponentOfTheTransformedGraph,
  getNodes,
  graphToSimpleForm,
  getEdges,
  getTreesDFS,
  BFS,
  awesomeGraph,
} from '../../simple-graph2/index';
import AdjacencyList from '../../components/AdjacencyList';
import GraphInfo from '../../components/GraphInfo';
import AdjacencyMatrix from '../../components/AdjacencyMatrix';
import ComponentsOfTheTransformedGraph from "../../components/ComponentsOfTheTransformedGraph";
import GraphAwesome from "../../components/GraphAwesome";
import Trees from "../../components/Trees";
import PropTypes from "prop-types";

class Lab1 extends Component {
  static propTypes = {
    content : PropTypes.string,
  };

  render() {
    const { content } = this.props;
    return (
      <div className="App">
        {content && <AdjacencyList
          adjacencyList={createAdjacencyList(content)}
          title="Исходный граф"
        />}
        {content && <AdjacencyMatrix adjacencyMatrix={stringToAdjacencyMatrix(content)} />}
        {content && <GraphInfo content={content} />}
        {content && <AdjacencyList
          adjacencyList={awesomeGraph(content)}
          title="Простой граф"
        />}
        {content && <ComponentsOfTheTransformedGraph
          array={findComponentOfTheTransformedGraph(content)}
        />}
        {content && <GraphAwesome
          title="Простой граф"
          graph={{
            nodes: getNodes(graphToSimpleForm(content)),
            edges: getEdges(graphToSimpleForm(content)),
          }}
        />}
        {content && <Trees array={getTreesDFS(content)} />}
        {content && <Trees array={BFS(content)} />}
      </div>
    );
  }
}

export default Lab1;
