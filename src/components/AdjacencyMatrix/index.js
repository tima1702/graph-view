import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class AdjacencyMatrix extends Component {
  static propTypes = {
    adjacencyMatrix : PropTypes.arrayOf(PropTypes.array),
  };

  render() {
    const { adjacencyMatrix } = this.props;
    return (
      <div className="container">
        <table>
          <tbody>
          {adjacencyMatrix.map(i => (
            <tr>
              {i.map(v => (<td>{v}</td>))}
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    );
  }
}
