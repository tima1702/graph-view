import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GraphAwesome from '../GraphAwesome';
import {getEdges, getNodes, graphToSimpleForm} from "../../simple-graph2";

class Trees extends Component {
  static propTypes = {
    array : PropTypes.arrayOf(PropTypes.object),
  };

  render() {
    const { array } = this.props;
    return (
      <div className="container">
        <h3>Островные деревья</h3>
        {array.map(i => (
          <div>
            <p>{`Компонента связоности: [ ${[...i.keys()].join(', ')} ]`}</p>
            <GraphAwesome title="Простой граф"
                          graph={{
                            nodes: getNodes(i),
                            edges: getEdges(i),
                          }}
            />
          </div>
        ))}
      </div>
    );
  }
}

export default Trees;
