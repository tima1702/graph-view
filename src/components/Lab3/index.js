import React, { Component } from 'react';
import {
  getNodesWithSize,
  stringToAdjacencyListVithSize,
  getEdgesWithSize,
  myPrimMST,
  myKruskal
} from '../../simple-graph2/index';
import PropTypes from "prop-types";
import GraphAwesome from "../GraphAwesome";

class Lab3 extends React.Component {
  render() {
    const { content } = this.props;
    const { result } = this.state;
    const arr = myPrimMST(content).map(a => `${a[0]}, ${a[1]}`);
    return (
      <div className="App">

      </div>
    );
  }
}

export default Lab3;
