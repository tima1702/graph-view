import React, { Component } from 'react';
import './App.css';
import Lab2 from './components/Lab2';

class App extends Component {
  constructor() {
    super();
    this.state = {
      content: null,
    };
  }
  load(e) {
    const file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = ((r) => {
      return () => {
        const content = r.result;
        this.setState({ content });
      };
    })(reader);

    reader.readAsText(file);
  }

  render() {
    const { content } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Lab 1</h1>
        </header>
        <input onChange={e => this.load(e)} type="file" />
        {content && <Lab2 content={content} />}
      </div>
    );
  }
}

export default App;
